#!/usr/bin/ruby

require "./app/view_event_handler"
require "./app/calculator_controller"
require "./app/cli_view"
require "./app/rpn_calculator"
require "./app/calculator_history"
require "./app/input_parser"

calculator = RPNCalculator.new
history = CalculatorHistory.new
controller = CalculatorController.new(calculator, history)
event_handler = ViewEventHandler.new(controller)
parser = InputParser.new(RPNCalculator.available_operators)
view = CLIView.new(parser, event_handler)

loop{$><<">> ";gets;$><<'=> ';puts view.read $_}
