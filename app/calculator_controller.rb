class CalculatorController
  private
  attr :calculator, :history

  public

  def initialize(calculator, history)
    @calculator = calculator
    @history = history
  end

  def handle_input(input_list)
    input_list.each{ |i| history.add(i) }

    return calculator.execute(history)
  end
end
