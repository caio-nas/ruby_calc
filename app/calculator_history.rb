require_relative 'exception/calculation_out_of_bounds_error'

class CalculatorHistory
  private
  attr :stack

  public

  def initialize(history = [])
    @stack = history
  end

  def current
    @stack[-1]
  end

  def pop
    @stack.pop.tap{ |e| raise CalculationOutOfBoundsError if e.nil? }
  end

  def add(input)
    @stack << input
    return self
  end
end
