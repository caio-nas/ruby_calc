require_relative "calculator_error"

class UnrecognizedTokenError < CalculatorError
  def initialize(token)
    super("Unrecognized token: #{token}")
  end
end
