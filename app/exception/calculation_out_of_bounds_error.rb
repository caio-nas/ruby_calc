require_relative "calculator_error"

class CalculationOutOfBoundsError < CalculatorError
  def initialize(msg="There's not enough tokens to execute this operation")
    super
  end
end
