require_relative 'exception/unrecognized_token_error'
require 'bigdecimal/util'
require 'bigdecimal'

class InputParser
  private
  #Decimal number regex from Rubinius(github.com/rubinius/rubinius)
  @@is_number ||= /^\s*[+-]?((\d+_?)*\d+(\.(\d+_?)*\d+)?|\.(\d+_?)*\d+)(\s*|([eE][+-]?(\d+_?)*\d+)\s*)$/
  attr :is_operator

  public

  def initialize(operator_regex)
    @is_operator = operator_regex
  end

  def parse(input_string)
    case input_string
    when @@is_number
      input_string.to_d
    when @is_operator
      input_string.to_sym
    else
      raise UnrecognizedTokenError, input_string
    end
  end
end
