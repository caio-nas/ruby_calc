class RPNCalculator
  @@available_operators ||= /[\+\-\*\/]/
  def self.available_operators
    @@available_operators
  end

  def execute(input_history)
    evaluate(input_history)
  end

  private

  def evaluate(history)
    if history.current =~ @@available_operators
      operator = history.pop

      x = evaluate(history)
      history.pop

      y = evaluate(history)
      history.pop

      history.add(calculate(operator, x, y))
    end

    return history.current
  end

  def calculate(operator, x, y)
    x.send(operator, y)
  end
end
