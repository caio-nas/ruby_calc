class ViewEventHandler
  private
  attr :listener

  public

  def initialize(listener)
    @listener = listener
  end

  def handle_input(input)
    @listener.handle_input(input)
  end
end
