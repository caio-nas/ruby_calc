require_relative "exception/calculator_error"

class CLIView
  def initialize(input_parser, view_event_handler)
    @parser = input_parser
    @event_hanlder = view_event_handler
  end

  def read(input)
    begin
      input = input && input.strip
      exit(1) if input.nil? or "q".eql?(input)
      tokens = input.split(" ").map{ |i| @parser.parse(i) }

      return tokens.empty? ? "" : "%.2f" % @event_hanlder.handle_input(tokens)
    rescue CalculatorError => e
      "Error: #{e.message}"
    end
  end
end
