require_relative "../app/input_parser"
require 'bigdecimal'

shared_examples "number parsing" do |input, expected|
  let(:output) { parser.parse input }

  it "parses output to BigDecimal" do
    expect(output).to be_a BigDecimal
  end

  context "when positive" do
    it "returns the same positive value" do
      expect(output).to eq expected
    end
  end

  context "when negative" do
    let(:output) { parser.parse input.prepend("-") }

    it "returns the same negative value" do
      expect(output).to eq (expected * -1)
    end
  end
end

shared_examples "operator parsing" do |input, expected|
  it "returns parsed operator symbol :#{expected} from #{input}" do
    expect(parser.parse input).to eq expected
  end
end

describe InputParser do
  let(:parser) { InputParser.new(/[\+\-\*\/]/) }

  describe "#parse" do
    context "when the input string has a integer number format" do
      include_examples "number parsing", "1043", 1043
    end

    context "when the input string has a decimal number format" do
      include_examples "number parsing", "10.43", 10.43
    end

    context "when the input string is a operator" do
      { "+" => :+, "-" => :-,  "*" => :*, "/" => :/ }.each do |k, v|
        include_examples "operator parsing", k, v
      end
    end

    it "raises UnrecognizedTokenError when anything else is given" do
      expect{ parser.parse("bla") }.to raise_error(UnrecognizedTokenError, "Unrecognized token: bla")
    end
  end
end
