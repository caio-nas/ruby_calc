require_relative "../app/calculator_history"
require_relative "../app/rpn_calculator"
require 'bigdecimal/util'

shared_examples "execute" do |objective, token_list, expected|
  it "#{objective}" do
    input_history = CalculatorHistory.new(token_list)
    result = calculator.execute(input_history)

    expect(result).to eq expected
    expect(input_history.current).to eq expected
  end
end

describe RPNCalculator do
  let(:calculator) { RPNCalculator.new }

  describe "#execute" do
    context "basic operations" do
      include_examples("execute", "'1 2 +' equals 3",
        [1, 2, :+], 3)

      include_examples("execute", "'1.1 2.7 +' equals 3.8",
        ["1.1".to_d, "2.7".to_d, :+], 3.8)

      include_examples("execute", "'5 4 -' equals -1",
        [5, 4, :-], -1)

      include_examples("execute", "'-0.1 -0.5 -' equals -0.4",
        ["-0.1".to_d, "-0.5".to_d, :-], -0.4)

      include_examples("execute", "'9 9 *' equals 81",
        [9, 9, :*], 81)

      include_examples("execute", "'100 -0.2 *' equals -20",
        [100, "-0.2".to_d, :*], -20)

      include_examples("execute", "'10 2 /' equals 0.2",
        ["10".to_d, 2, :/], 0.2)

      include_examples("execute", "'0.7 0.3 /' equals 0.428571429",
        ["0.7".to_d, "0.3".to_d, :/], 0.428571429)
    end

    context "combined operations" do
      include_examples("execute", "'1 1 2 - -' equals 0",
        [1, 1, 2, :-, :-], 0)

      include_examples("execute", "'3 0.5 12 / *' equals 72",
        [12, "0.5".to_d, 3, :/, :*], 72)

      include_examples("execute", "'3 4 2 10 - + /' equals 4",
        [3, 4, 2, 10, :-, :+, :/], 4)
    end

    context "nested operations" do
      include_examples("execute", "'3 5 * 2 -' equals -13",
        [3, 5, :*, 2, :-], -13)

      include_examples("execute", "'0.5 3 / 0.5 *' equals 3",
        ["0.5".to_d, 3, :/, "0.5".to_d, :*], 3)

      include_examples("execute", "'3 4 2 * 10 - +' equals 5",
        [3, 4, 2, :*, 10, :-, :+], 5)
    end
  end
end
