require_relative "../app/calculator_history"

describe CalculatorHistory do
  let(:history) { CalculatorHistory.new }

  describe "#initialize" do
    it "initilizes an empty history when called without arguments" do
      expect(history.current).to be_nil
    end

    it "initilizes a loaded history when called with a list argument" do
      assert_history_consume(CalculatorHistory.new([1, 2, :*, 4, :+]), [:+, 4, :*, 2, 1])
    end
  end

  describe "#add" do
    it "adds a new input on the top of the history" do
      history.add(1)

      assert_history_consume(history, [1])
    end

    it "accepts elements with the same value" do
      history.add(2)
             .add(2)
             .add(:+)

      assert_history_consume(history, [:+, 2, 2])
    end
  end

  describe "#current" do
    it "returns most recent item in the history" do
      history.add(3)
             .add(4)

      expect(history.current).to eq 4
    end

    it "does not affect history contents, even with multiple calls" do
      history.add(5)

      expect(history.current).to eq 5
      expect(history.current).to eq 5
    end

    it "returns nil when the history is empty" do
      expect(history.current).to be_nil
    end
  end

  describe "#pop" do
    it "returns most recent item in the history" do
      history.add(3)
             .add(4)
             .add(:/)

      expect(history.pop).to eq :/
    end

    it "removes the returned element from history" do
      history.add(5)

      expect(history.pop).to eq 5
      expect(history.current).to be_nil
    end

    it "raises CalculationOutOfBoundsError when the history is empty" do
      expect{ history.pop }.to raise_error(CalculationOutOfBoundsError)
    end
  end

  def assert_history_consume(history, expected_values)
    expected_values.each do |v|
      expect(history.current).to eq v
      expect(history.pop).to eq v
    end
  end

end
