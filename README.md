CLI RPN Calculator
==================

Simple command-line reverse polish notation (RPN) made with plain Ruby

Architecture
-----------------

The app is based on the MVC pattern, organized on loosely coupled classes intended to be reused on diverse scenarios (e.g. another calculator implementation or web views). For more details on how the most important classes work, check out the `spec` folder

Dependencies
--------------

- Ruby v2.3.3
- Rspec gem for tests

How to run
--------------
`ruby main.rb`

Example Input/Output
--------------------

The calculator accepts standard and inline formats:

    >> -0.1
    => -0.10
    >> -0.5
    => -0.50
    >> -
    => -0.40

---

    >> 1 2 +
    => 3.00

---

More examples:

    basic operations
      '1.1 2.7 +' equals 3.8
      '5 4 -' equals -1
      '9 9 *' equals 81
      '100 -0.2 *' equals -20
      '10 2 /' equals 0.2
      '0.7 0.3 /' equals 0.428571429
    combined operations
      '1 1 2 - -' equals 0
      '3 0.5 12 / *' equals 72
      '3 4 2 10 - + /' equals 4
    nested operations
      '3 5 * 2 -' equals -13
      '0.5 3 / 0.5 *' equals 3
      '3 4 2 * 10 - +' equals 5
---


Next revisions
--------------------

- Add more operations
- GUI tests
